import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    login: false
  },
  getters: {
    getLogin (state) {
      return state.login
    } 
  },
  mutations: {
    setLogin (state, payload)  {
      state.login = payload
    }
  },
  actions: {
    setLogin({commit}, payload) {
      commit("setLogin", payload)
    }
  },
  modules: {
  }
})
