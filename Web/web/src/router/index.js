import store from "@/store";
import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
 
  {
    path: '',
    name: 'toolbar',
    component: () => import("../views/Toolbar.vue"),
    children: [
      {
        path: "/",
        name: "register",
        component: () => import("../views/Register.vue"),
      },
      {
        path: "/login",
        name: "login",
        component: () => import("../views/Login.vue"),
      },

      {
        path: "/home",
        name: "home",
        meta: {
          requireAuth: true
        },
        component: () => import("../views/Home.vue"),
      },
      {
        path: "/billsaleadmin",
        name: "billsaleadmin",
        meta: {
          requireAuth: true
        },
        component: () => import("../views/BillSaleAdmin.vue"),
      },
      {
        path: "/orderhistory",
        name: "orderhistory",
        meta: {
          requireAuth: true
        },
        component: () => import("../views/OrderHistory.vue"),
      },
    ],

  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requireAuth)) {
    if (store.getters.getLogin) {
      next()
      return 
    }
    next({name: "login"})
  } else {
    next()
  }
})

export default router;
