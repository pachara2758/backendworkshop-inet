const express = require("express");
const router = express.Router();
const AutoIncrementModel = require("../models/AutoIncrementModel");
const ProductModel = require("../models/ProductModel");

async function autoIncrement(collectionName) {
  let primaryKey;
  let result = await AutoIncrementModel.findOneAndUpdate(
    { collectionName: collectionName },
    { $inc: { id: 1 } },
    { new: true, upsert: true } ////upsert: true option is used to create the document if it doesn't exist.
  );

  if (result === null) {
    const newVal = new AutoIncrementModel({
      collectionName: collectionName,
      id: 1,
    });
    await newVal.save();
    primaryKey = 1;
  } else {
    primaryKey = result.id;
  }
  return primaryKey;
}

router.post("/", async (req, res) => {
  try {
    let { productId, name, detail, cost, amount } = req.body;
    let checkProduct = await ProductModel.find({ productId, detail });
    let product;
    if (checkProduct.length === 0) {
      let collectionName = "products";
      let primaryKey = await autoIncrement(collectionName);
      const newProduct = new ProductModel({
        id: primaryKey,
        productId,
        name,
        detail,
        cost,
        amount,
      });
      product = await newProduct.save();
    } else {
      product = checkProduct[0];
      let id = product.id;
      let oldAmount = product.amount;
      let newAmount = oldAmount + amount;

      await ProductModel.updateOne(
        { id: id },
        {
          $set: {
            amount: newAmount,
          },
        }
      );
      product = await ProductModel.find({ id: id }).select("-_id -__v");
      product = product[0];
      console.log(product);
    }

    return res.status(200).send({
      data: {
        id: product.id,
        productId: product.productId,
        name: product.name,
        detail: product.detail,
        cost: product.cost,
        amount: product.amount,
      },
      message: "create success",
      success: true,
    });
  } catch (e) {
    return res.status(500).send({
      message: "create fail",
      // message: e.message,
      success: false,
    });
  }
});

router.get("/", async (req, res) => {
  try {
    let products = await ProductModel.find().select("-_id -__v").sort({ productId: 1 });
    return res.status(200).send({
      data: products,
      message: "success",
      success: true,
    });
  } catch (e) {
    return res.status(500).send({
      message: e.message,
      success: false,
    });
  }
});

router.get("/:productId", async (req, res) => {
  try {
    let productId = req.params.productId;
    let product = await ProductModel.find({ productId: productId }).select(
      "-_id -__v"
    );
    let totalAmount = 0;
    await product.map((item) => {
      totalAmount += item.amount;
    });
    return res.status(200).send({
      data: product,
      totalAmount,
      message: "success",
      success: true,
    });
  } catch (e) {
    res.status(500).send({
      message: e.message,
      success: false,
    });
  }
});

router.delete("/:productId", async (req, res) => {
  try {
    let productId = req.params.productId;
    await ProductModel.deleteMany({ productId: productId });

    return res.status(200).send({
      message: "delete success",
      success: true,
    });
  } catch (e) {
    res.sendStatus(500).send(e.message);
  }
});

router.delete("/deleteOne/:id", async (req, res) => {
  try {
    let id = req.params.id;
    await ProductModel.deleteOne({ id: id });

    return res.status(200).send({
      message: "delete success",
      success: true,
    });
  } catch (e) {
    res.sendStatus(500).send(e.message);
  }
});

router.put("/:id", async (req, res) => {
  try {
    let id = req.params.id;
    let body = req.body;
    await ProductModel.updateOne(
      { id: id },
      {
        $set: {
          productId: body.productId,
          name: body.name,
          detail: body.detail,
          cost: body.cost,
          amount: body.amount,
        },
      }
    );

    let product = await ProductModel.find({ id: id }).select("-_id -__v");
    return res.status(200).send({
      data: product,
      message: "update success",
      success: true,
    });
  } catch (e) {
    return res.status(500).send({
      message: e.message,
      success: false,
    });
  }
});

router.get("/distinct/product", async (req, res) => {
  try {
    // let products = await ProductModel.distinct("productId");

    const product = await ProductModel.aggregate([
      {
        $group: {
          _id: "$productId",
          name: { $first: "$name" },
          cost: { $first: "$cost" },
          amount: { $sum: "$amount"}
        },
      },
    ]);
    return res.status(200).send({
      data: product,
      message: "success",
      success: true,
    });
  } catch (e) {
    return res.status(500).send({
      message: e.message,
      success: false,
    });
  }
});

module.exports = router;
