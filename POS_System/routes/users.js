var express = require('express');
var router = express.Router();

const bcrypt = require('bcrypt');
const AutoIncrementModel = require('../models/AutoIncrementModel');
const UserModel = require("../models/UserModel")
const jwt = require('jsonwebtoken')
require('dotenv').config
const service = require('./service')




async function autoIncrement(collectionName) {
  let primaryKey
  let result = await AutoIncrementModel.findOneAndUpdate(
    { collectionName: collectionName },
    { $inc: { id: 1 }},
    { new: true, upsert: true } ////upsert: true option is used to create the document if it doesn't exist.
  );
  
  if (result === null) {
    const newVal = new AutoIncrementModel({ collectionName: collectionName, id: 1 });
    await newVal.save();
    primaryKey = 1;
  } else {
    primaryKey = result.id;
  }
  return primaryKey
}

router.post('/register', async (req, res) => {
  try {
    let collectionName = "users"
    let primaryKey =  await autoIncrement(collectionName)
    let { username, password, firstName, lastName, age, gender } = req.body;
    let hashPassword = await bcrypt.hash(password, 10);
    const newUser = new UserModel({
      userId: primaryKey,
      username,
      password: hashPassword,
      firstName,
      lastName,
      age,
      gender
    });

    const user = await newUser.save();
    return res.status(200).send({
      data: { _id: user._id, userId: primaryKey, username, firstName, lastName, age, gender },
      message: "create success",
      success: true
    })

  } catch (e) {
    return res.status(500).send({
      message: "create fail",
      success: false
    });
  }
});

router.get('/', async (req, res) => {
  try {
    const users = await UserModel.find().select('-password -_id -__v');

    return res.status(200).send({
      data: users,
      message: "success",
      success: true
    })
  } catch (e) {

    return res.status(500).send({
      message: "fail",
      success: false
    })
  }
})

router.get('/:id', async (req, res) => {
  try {
    const id = req.params.id
    const users = await UserModel.find({userId: id}).select('-password -_id -__v');

    return res.status(200).send({
      data: users,
      message: "success",
      success: true
    })
  } catch (e) {

    return res.status(500).send({
      message: "fail",
      success: false
    })
  }
})

router.get('/auth/login', async (req, res) => {
  try {
    const token = req.headers.authorization.split("Bearer ")[1]
    const detoken = jwt.verify(token, process.env.secret_key)
    return res.status(200).send({
      data: detoken,
      success: true
    })
  } catch(e) {
    return res.status(500).send({
      message: "fail",
      success: false
    })
  }
})

router.put('/:id', async (req, res) => {
  try {
    let id = req.params.id;
    let body = req.body;
    let password = body.password
    if (password != undefined && password.length > 0) {
      let hashPassword = await bcrypt.hash(password, 10);
      await UserModel.updateOne(
        {userId: id},
        { $set: {
          username: body.username,
          password: hashPassword,
          firstName: body.firstName,
          lastName: body.lastName,
          age: body.age,
          gender: body.gender
        }}
      );
      
    } else {
      await UserModel.updateOne(
        {userId: id},
        { $set: {
          username: body.username,
          firstName: body.firstName,
          lastName: body.lastName,
          age: body.age,
          gender: body.gender
        }}
      );
    }
    let user = await UserModel.find({userId: id}).select('-password -_id')
    res.status(200).send({
      data: user,
      message: "update success",
      success: true
    })
  }catch (e) {
    res.status(500).send({
      message: "update fail",
      success: false
    })
  }
})

router.delete('/:id', async (req, res) => {
  try {
    let id = req.params.id

    await UserModel.deleteOne({ userId: id });
    return res.status(200).send({
      message: "delete success",
      success: true
  })
  } catch (e) {
    return res.status(500).send({
      message: "delete fail",
      success: false
    })
  }
  
})

let checkToken = (req, res, next) => {
  try {
    console.log(`${req.headers.authorization}`)
    let token = req.headers.authorization.split('Bearer ')[1]
    let detoken = jwt.verify(token, process.env.secret_key)
    req.detoken = detoken
    next()
  } catch(e) {
    res.status(401).send({
      message: e.message
    })
  }

}
router.post('/login', async (req, res) => {
  try {
    let { username, password } = req.body
    let payload = {}
    let token = {}
    let user = await UserModel.findOne({
      username
    });
    let checkUser = await bcrypt.compare(password, user.password);
    if(checkUser) { 
      payload.userId = user.userId
      payload.firstName = user.firstName;
      payload.lastName = user.lastName;
      payload.role = user.role
      token = jwt.sign(payload, process.env.secret_key, {
        expiresIn: "7d"
      });
    }
    else {
      throw { message: "password is wrong", status: 400}
    }

    // let detoken = jwt.verify(token, process.env.secret_key)

    
    res.send({
      data: token,
      message: "success",
      success: true
    })

  } catch (e) {
    res.status(e.status || 500).send({
      message: "user not found",
      status: e.status,
      success: false
    })
  }
})


module.exports = router;
