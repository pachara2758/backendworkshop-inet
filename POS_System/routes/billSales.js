const express = require("express");
const router = express.Router();
const BillSaleModel = require("../models/BillSaleModel");
const AutoIncrementModel = require("../models/AutoIncrementModel");
const UserModel = require("../models/UserModel");
const ProductModel = require("../models/ProductModel");

async function autoIncrement(collectionName) {
  let primaryKey;
  let result = await AutoIncrementModel.findOneAndUpdate(
    { collectionName: collectionName },
    { $inc: { id: 1 } },
    { new: true, upsert: true } ////upsert: true option is used to create the document if it doesn't exist.
  );

  if (result === null) {
    const newVal = new AutoIncrementModel({
      collectionName: collectionName,
      id: 1,
    });
    await newVal.save();
    primaryKey = 1;
  } else {
    primaryKey = result.id;
  }
  return primaryKey;
}

router.post("/", async (req, res) => {
  try {
    let collectionName = "billSales";
    let primaryKey = await autoIncrement(collectionName);
    let { userId, productList } = req.body;
    let now = new Date();
    let currDate = `${now.getFullYear()}${now.getMonth()+1}${now.getDate()}`
    let itemList = []
    let orderId = "TCPUDPID#" + currDate + String(primaryKey).padStart(4, "0");
    let purchaser = await UserModel.findOne({ userId });
    let totalAmount = 0;
    let billSale
    let result

    for (const item of productList) {

      let checkProduct = await ProductModel.findOne({
        productId: item.productId,
        detail: { size: item.size, color: item.color },
      });
      let totalProduct = checkProduct.amount;
      if (item.amount > totalProduct) {
        totalAmount = 0
        itemList = []
        throw new Error("The product is not enough"); // Throw an error here
      } else {
        let cost = checkProduct.cost
        let price = 0
        price = cost * item.amount
        totalAmount += price
        // totalAmount += cost * item.amount
        item.id = checkProduct.id
        item.price = price
        item.oldAmount = checkProduct.amount
        itemList.push(item)
      }
      
    }
    console.log(itemList)
    for (const item of itemList) {
      let product = await ProductModel.findOne({id: item.id})
      let newAmount = item.oldAmount - item.amount
      console.log(product.amount)
      console.log(newAmount)
      await ProductModel.updateOne({
        id: item.id
      }, {
        $set: {
          amount: newAmount
        }
      })
    }
    const newBillSale = await new BillSaleModel({
      id: primaryKey,
      orderId,
      purchaser: purchaser.firstName,
      productList: itemList,
      totalAmount
    })
    billSale = await newBillSale.save();
    result = await BillSaleModel.findOne({id: billSale.id}).select('-_id -__v -createdAt -updatedAt')

    return res.status(200).send({
        data: result,
        message: "create success",
        success: true
    });
  } catch (e) {
    return res.status(500).send({
      message: e.message,
      success: false,
    });
  }
});


router.get('/', async (req, res) => {
  try {
    const billSales = await BillSaleModel.find().select('-_id -__v');
    return res.status(200).send({
      data: billSales,
      message: "success",
      success: true
    })
  } catch (e) {
    return res.status(500).send({
      message: e.message,
      success: false
    })
  }
})

router.get('/:id', async (req, res) => {
  try {
    
    const id = req.params.id
    const billSales = await BillSaleModel.find({ id }).select('-_id -__v');
    return res.status(200).send({
      data: billSales,
      message: "success",
      success: true
    })
  } catch (e) {
    return res.status(500).send({
      message: e.message,
      success: false
    })
  }
})

router.get('/only/:userId', async (req, res) => {
  try {
    
    const id = req.params.userId
    const purchaser = await UserModel.findOne({ userId: id}).select('firstName')
    const billSales = await BillSaleModel.find({purchaser: purchaser.firstName}).select('-_id -__v');
    return res.status(200).send({
      data: billSales,
      message: "success",
      success: true
    })
  } catch (e) {
    return res.status(500).send({
      message: e.message,
      success: false
    })
  }
})


module.exports = router;
