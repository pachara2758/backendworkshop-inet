const mongoose = require('mongoose');
const UserModel = new mongoose.Schema({
    userId: { type: Number, unique: true, require: true },
    username: { type: String, unique: true, require: true },
    password: { type: String, require: true },
    firstName: { type: String, default: "" },
    lastName: { type: String, default: "" },
    age: { type: Number, default: 0 },
    gender: { type: String, default: ""},
    role: { type: String, default: "user"},
})

module.exports = mongoose.model('users', UserModel)