const mongoose = require('mongoose');
const BillSaleModel = mongoose.Schema({
    id: { type: Number, unique: true, require: true },
    orderId: { type: String },
    purchaser: { type: String },
    productList: { type: Array, require: true },
    totalAmount: { type: Number,  }
}, 
{ timestamps: true }
)

module.exports = mongoose.model('billSales', BillSaleModel);