const mongoose = require('mongoose');
let AutoIncrementModel = mongoose.Schema({
    collectionName: { type: String },
    id: { type: Number }
})

module.exports = mongoose.model('autoIncrement', AutoIncrementModel);