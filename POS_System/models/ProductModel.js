const mongoose = require('mongoose');
const ProductModel = new mongoose.Schema({
    id: { type: Number, unique: true, require: true },
    productId: { type: String, require: true },
    imgLink: { type: String, default: ""},
    name: { type: String, require: true },
    detail: { type: Object },
    cost: { type: Number, require: true },
    amount: { type: Number, require: true }

})



module.exports = mongoose.model('products', ProductModel);